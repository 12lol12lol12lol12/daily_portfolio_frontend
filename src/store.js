import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    auth_token:
      localStorage.getItem("token") === "null" ||
      localStorage.getItem("token") === null
        ? null
        : localStorage.getItem("token"),
    username:
      localStorage.getItem("username") === "null" ||
      localStorage.getItem("username") === null
        ? null
        : localStorage.getItem("username"),
    email:
      localStorage.getItem("email") === "null" ||
      localStorage.getItem("email") === null
        ? null
        : localStorage.getItem("email"),
    first_analyze: null,
    num_scs: null,
    a_balance: null,
    a_balance_headers: null,
    dates: null
  },
  getters: {
    TOKEN: state => {
      return state.auth_token;
    },
    USERNAME: state => {
      return state.username;
    },
    EMAIL: state => {
      return state.email;
    },
    isAuthenticated: state => !!state.auth_token,
    FIRST_ANALYZE: state => {
      return state.first_analyze;
    },
    NUM_SCS: state => {
      return state.num_scs;
    },
    A_BALANCE: state => {
      return state.a_balance;
    },
    A_BALANCE_HEADERS: state => {
      return state.a_balance_headers;
    },
    DATES: state => {
      return state.dates;
    }

  },
  mutations: {
    SET_TOKEN: (state, newToken) => {
      state.auth_token = newToken;
      localStorage.setItem("token", newToken);
    },
    SET_USERNAME: (state, newUsername) => {
      state.username = newUsername;
      localStorage.setItem("username", newUsername);
    },
    SET_EMAIL: (state, newEmail) => {
      state.email = newEmail;
      localStorage.setItem("state", newEmail);
    },
    SET_FIRST_ANALYZE: (state, newFirstAnalyze) => {
      state.first_analyze = newFirstAnalyze;
    },
    SET_NUM_SCS: (state, newN) => {
      state.num_scs = newN;
    },
    SET_A_BALANCE: (state, newN) => {
      state.a_balance = newN;
    },
    SET_A_BALANCE_HEADERS: (state, newN) => {
      state.a_balance_headers = newN;
    },
    SET_DATES: (state, newN) => {
      state.dates = newN;
    },
  },
  actions: {
    SET_TOKEN: (context, newToken) => {
      context.commit("SET_TOKEN", newToken);
    },
    SET_USERNAME: (context, newUsername) => {
      context.commit("SET_USERNAME", newUsername);
    },
    SET_EMAIL: (context, newEmail) => {
      context.commit("SET_EMAIL", newEmail);
    },
    SET_FIRST_ANALYZE: (context, newFirstAnalyze) => {
      context.commit("SET_FIRST_ANALYZE", newFirstAnalyze);
    },
    SET_NUM_SCS: (context, newN) => {
      context.commit("SET_NUM_SCS", newN);
    },
    SET_A_BALANCE: (context, newN) => {
      context.commit("SET_A_BALANCE", newN);
    },
    SET_A_BALANCE_HEADERS: (context, newN) => {
      context.commit("SET_A_BALANCE_HEADERS", newN);
    },
    SET_DATES: (context, newN) => {
      context.commit("SET_DATES", newN);
    },
    logoutUser: context => {
      context.commit("SET_TOKEN", null);
      context.commit("SET_USERNAME", null);
      context.commit("SET_EMAIL", null);
    },
    

    isAuth: context => {
      if (context.getters.TOKEN === null) {
        return false;
      }
      return true;
    }
  } 
});
