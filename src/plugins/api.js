import axios from 'axios'

export const API = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL
})

export const AUTHORIZED_API = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL
})
