import Vue from "vue";
import Router from "vue-router";
import LoginComponent from "@/components/LoginComponent";
import SignUpComponent from "@/components/SignUpComponent";
import MainPageComponent from "@/components/MainPageComponent";
import LoadDataExcel from "@/components/LoadDataExcel";
import ReportComponent from "@/components/ReportComponent";
import ConstructorComponent from "@/components/ConstructorComponent";
import MainComponent from "@/components/MainComponent";
import GraphComponent from "@/components/GraphComponent";
import { store } from "../store";

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
  if (store.getters.TOKEN === null) {
    next("/");
  } else {
    next();
    return;
  }
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.TOKEN != null) {
    next("main/graph");
  } else {
    next();
    return;
  }
};

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      component: LoginComponent,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/sign_up",
      component: SignUpComponent,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/main",
      component: MainPageComponent,
      beforeEnter: ifNotAuthenticated,
      children: [
        {
          path: "home",
          component: MainComponent,
          beforeEnter: ifNotAuthenticated
        },
        {
          path: "load_excel",
          component: LoadDataExcel,
          beforeEnter: ifNotAuthenticated,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "report",
          component: ReportComponent,
          beforeEnter: ifNotAuthenticated
        },
        {
          path: "constructor",
          component: ConstructorComponent,
          beforeEnter: ifNotAuthenticated
        },
        {
          path: "graph",
          component: GraphComponent,
          beforeEnter: ifNotAuthenticated
        }
      ]
    }
  ]
});

export default router;
