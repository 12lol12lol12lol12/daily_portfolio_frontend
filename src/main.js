// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import vuetify from '@/plugins/vuetify'
import router from './router'
import Vuex from 'vuex'
import {store} from './store'
import Notifications from 'vue-notification'
import VueGoogleCharts from 'vue-google-charts'

Vue.use(Vuex)
Vue.use(Notifications)
Vue.use(VueGoogleCharts)
Vue.config.productionTip = false
console.log(process.env.VUE_APP_BASE_URL)

/* eslint-disable no-new */
new Vue({
  router,
  vuetify,
  el: '#app',
  components: { App },
  template: '<App/>',
  store
})

Vue.filter('formatNumber', function (value) {
  if(value === undefined)
  {
    return '0';
  }
  return value.toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
});
